class UsersController < ApplicationController

	def new
		@user=User.new
	end

	def index
	end

	def create
		@user=User.new(user_params)
		if @user.save
			flash[:now]= "Created!"
			redirect_to sessions_login_path
		else
			render 'new'
		end
	end

	before_filter :save_login_state, :only => [:new, :create]

	private
	def user_params
		params.require(:user).permit(:name, :password, :password_confirmation)
	end
end
